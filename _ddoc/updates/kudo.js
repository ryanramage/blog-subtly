function kudo(doc, req) {
    if (!doc && req.form && req.body) {
        try {
            var body = JSON.parse(req.body);

            if (body.post_id) return [{
                _id: req.uuid,
                post_id: body.post_id,
                kudos: true,
                timestamp: new Date().getTime(),
                headers: req.headers,
                peer: req.peer,
                userCtx: req.userCtx,
                cookie: req.cookie
            }, 'true'];

        } catch(e) {}
    }
    return [null, 'false'];

}