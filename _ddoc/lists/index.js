function index(head, req) {
  var row;
  start({
    "headers": {
      "Content-Type": "text/html"
     }
  });
  send(this.templates.header);

  var Mustache = require("lib/mustache"),
      marked = require("lib/marked"),
      moment = require("lib/moment.min"),
      slug = require("views/lib/slug");

  marked.setOptions({
    gfm: true,
    tables: true,
    breaks: false,
    pedantic: false,
    sanitize: true,
    smartLists: true,
    langPrefix: 'language-',
    highlight: function(code, lang) {
      return code;
    }
  });

  var one_entry = (req.query && req.query.key);


  while(row = getRow()) {
    var p_slug = row.doc.slug;
    if (!p_slug) {
      p_slug = slug(row.doc.title);
    }
    var post = {
        _id: row.doc._id,
        slug: p_slug,
        title: row.doc.title || row.doc.date,
        entry: marked(row.doc.entry),
        date: moment(row.doc.date).fromNow(),
        one_entry : one_entry
    };
    // check for an image attachment
    if (row.doc._attachments) {
      for (var key in row.doc._attachments) {
          if (endsWith(key, '.jpg')) {
            post.img = ['.', '_img', post._id, key].join('/');
          }
      }
    }


    send(Mustache.render(this.templates.post, post));
  }

  send(this.templates.footer);

  function endsWith(str, suffix) {
      return str.indexOf(suffix, str.length - suffix.length) !== -1;
  }
}