function map(doc) {
    if (doc.entry && doc.slug) {
        return emit(doc.slug, doc.title);
    }
    if (doc.entry && doc.title) {
        var slug = require('views/lib/slug');
        return emit(slug(dog.title), doc.title);
    }
}