module.exports = slug;
module.exports.display_slug = display_slug;

function slug(title) {
    if (!title) return title;
    title.replace(' ', '-');
}

function display_slug(slug) {
    if (!slug) return slug;
    slug.replace('-', ' ');
}