define(['jquery', 'svg', 'text!img/icon.path', 'couchr', 'store'], function($, svg, icon, couchr, store) {

    return function(options) {
        var paper = svg(options.element).size(options.size, options.size);
        var main_path = paper.path(icon);
        main_path.size(options.size, options.size);
        main_path.attr({ fill: '#222' });
        $(setupKudos);
    };


    function setupKudos() {
        var keys = {};
        $('.post .kudos').each(function(i, elem){
            var $elem = $(elem);
            var id = $elem.data('id');
            if (keys[id]) keys[id].push($elem);
            else keys[id] = [$elem];
        });
        var query_keys = Object.keys(keys);
        couchr.get('./_ddoc/_view/kudos_by_post', {keys: query_keys, reduce: true, group: true}, function(err, resp){
            if (err) return;
            resp.rows.forEach(function(row){
                showKudo(row.key, keys[row.key], row.value);
                delete keys[row.key];
            });
            // leftovers show 0
            for (var key in keys) {
                showKudo(key, keys[key], 0);
            }
        });
    }

    function showKudo(id, elements, currentCount) {
        var given = store.get(id);
        if (given) return givenKudo(id, elements, currentCount);

        elements.forEach(function($elem){

            var $txt = $elem.find('.txt'),
                $cnt = $elem.find('.count'),
                $fil = $elem.find('.filling');

            $elem.css('visibility', 'visible')
                .hover(function(){
                    if ($elem.hasClass('counted')) return;
                    $fil.addClass('activated');
                    $txt.text('Please Wait...');
                },
                function(){
                    if ($elem.hasClass('counted')) return;
                    $fil.removeClass('activated');
                    $txt.text('Kudos');
                });
            $fil.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
               if (!$fil.hasClass('activated')) return;
               postKudo(id, elements, currentCount);
            });
            $cnt.text(currentCount);
        });
    }

    function postKudo(id, elements, currentCount) {
        couchr.post('./_kudo', {post_id: id}, function(err, resp){
            if (err || resp === 'false') return alert("Sorry, your Kudos count not be added :( ");
            store.set(id, true);
            givenKudo(id, elements, currentCount + 1);
        });
    }

    function givenKudo(id, elements, currentCount) {
        elements.forEach(function($elem){
            $elem.css('visibility', 'visible')
                .addClass('counted');

            var $txt = $elem.find('.txt'),
                $cnt = $elem.find('.count'),
                $chk = $elem.find('.check'),
                $fil = $elem.find('.filling');


            $fil.remove();
            $chk.show();

            $txt.text('Kudos');
            $cnt.text(currentCount);
        });
    }





});
